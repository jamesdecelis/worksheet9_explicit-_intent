package mt.edu.mcast.explicitintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final int SECOND_ACTIVITY_REQUEST_CODE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void askClicked(View v){

        EditText etxt_question = findViewById(R.id.etxt_question);
        String questionString = etxt_question.getText().toString();

        Intent i = new Intent(this, SecondActivity.class);

        i.putExtra("question", questionString);

        startActivityForResult(i, SECOND_ACTIVITY_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == SECOND_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){

            TextView txt_answer = findViewById(R.id.txtv_answer);
            txt_answer.setText(data.getExtras().getString("answer"));

        }

    }
}
