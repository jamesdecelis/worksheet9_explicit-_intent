package mt.edu.mcast.explicitintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView txt_question = findViewById(R.id.txtvQuestion);


        //reading extra info from intent and displaying the text within a textView
        Bundle extras = getIntent().getExtras();

        if(extras != null){
            String questionString = extras.getString("question");

            txt_question.setText(questionString);
        }

        Button btn = findViewById(R.id.btn_answer);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public  void finish(){

        Intent i =new Intent();

        EditText etxt_answer = findViewById(R.id.etxt_answer);
        String answerString = etxt_answer.getText().toString();

        i.putExtra("answer", answerString);

        setResult(RESULT_OK, i);

        super.finish();

    }

}
